1. Membuat database
CREATE DATABASE myshop;

2. Membuat table di dalam database
  2.1 Table Users
  CREATE TABLE users ( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

  2.2 Table Categories
  CREATE TABLE categories ( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null );

  2.3 Table Items
  CREATE TABLE items ( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );

3. Memasukkan data pada table
  3.1 Table Users
  INSERT INTO users (name, email, password) VALUES ("John Doe","john@doe.com","john123"), ("Jane Doe","jane@doe.com","jenita123");

  3.2 Table Categories
  INSERT INTO categories (name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

  3.3 Table Items
  INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1), ("Uniklooh","baju keren dari brand ternama",500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil data dari database
  4.a. Mengambil data users kecuali password
  SELECT id, name, email from users;

  4.b. Mengambil data items
    4.b.1 yang memiliki harga diatas 1000000 (satu juta)
    SELECT * FROM items WHERE price > 1000000;

    4.b.2 yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”
    SELECT * FROM items WHERE name LIKE 'uniklo%';
  
  4.c. Menampilkan data items join dengan kategori
  SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories on items.category_id = categories.id;

5. Mengubah daata dari database pada table items dengan nama sumsang b50 harganya (price) menjadi 2500000
UPDATE items SET price = 2500000 WHERE name = "Sumsang b50";

  
